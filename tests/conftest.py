from unittest.mock import MagicMock

import pytest

from netorca_sdk.auth import NetorcaAuth
from netorca_sdk.netorca import Netorca


@pytest.fixture
def auth_mock(mocker: MagicMock) -> MagicMock:
    mock = mocker.Mock(spec=NetorcaAuth)
    mock.fqdn = "https://api.dev.netorca.io/v1"
    return mock


@pytest.fixture
def netorca(auth_mock: NetorcaAuth) -> Netorca:
    return Netorca(auth_mock)
